﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float maxHealth = 100;
    public float maxShield = 100;
    public SlideBar healthBar;
    public SlideBar shieldBar;

    private float health;
    private float shield;


    private void Start()
    {
        health = maxHealth;
        healthBar.SetMaxValue(health);
        shield = maxShield;
        shieldBar.SetMaxValue(shield);
    }
    private void LateUpdate()
    {
        healthBar.SetValue(health);
        shieldBar.SetValue(shield);
    }
    public void Heal(float amount)
    {
        health += amount;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    public void RestoreShield(float amount)
    {
        shield += amount;
        if (shield > maxShield)
            shield = maxShield;
    }

    public void TakeDamage(float damage)
    {
        if (shieldBar != null)
        {
            if (shield <= 0)
                health -= damage;
            else
            {
                health -= damage * 0.2f;
                shield -= damage * 0.8f;
            }
        }
        else
            health -= damage;
        if (health <= 0) Die();

    }

    private void Die()
    {
        if (gameObject.tag == "Player")
            SceneManager.LoadScene("EndGame");
        else
            Destroy(gameObject);
    }


}
