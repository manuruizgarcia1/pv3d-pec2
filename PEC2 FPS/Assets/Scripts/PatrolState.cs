﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : IEnemyState
{
    EnemyAI myEnemy;
    private int nextWayPoint = 0;

    public PatrolState(EnemyAI enemy)
    {
        myEnemy = enemy;
    }

    public void UpdateState()
    {
        myEnemy.myLight.color = Color.green;

        myEnemy.agent.destination =
            myEnemy.wayPoints[nextWayPoint].position;

        if (myEnemy.agent.remainingDistance <=
            myEnemy.agent.stoppingDistance)
        {
            nextWayPoint = (nextWayPoint + 1) %
                myEnemy.wayPoints.Length;
        }
    }

    public void Impact()
    {
        GoToAttackState();
    }

    public void GoToAlertState()
    {
        myEnemy.agent.Stop();
        myEnemy.currentState = myEnemy.alertState;
    }

    public void GoToAttackState()
    {
        myEnemy.agent.Stop();
        myEnemy.currentState = myEnemy.attackState;
    }

    public void GoToPatrolState(){}

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
            GoToAlertState();
    }

    public void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
            GoToAlertState();
    }

    public void OnTriggerExit(Collider col){}
}
