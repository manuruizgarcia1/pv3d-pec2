﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    public Transform targetCam;

    private void LateUpdate()
    {
        transform.LookAt(transform.position + targetCam.forward);
    }
}
