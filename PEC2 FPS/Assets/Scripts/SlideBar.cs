﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;
   
    public void SetMaxValue(float value)
    {
        slider.maxValue = value;
        slider.value = value;

        if (gradient != null)
        {
            gradient.Evaluate(1f);
        }
    }

    public void SetValue(float value)
    {
        slider.value = value;

        if (gradient != null)
        {
            gradient.Evaluate(slider.normalizedValue);
        }
    }
}
