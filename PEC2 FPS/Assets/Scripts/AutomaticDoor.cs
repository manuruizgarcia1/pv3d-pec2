﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticDoor : MonoBehaviour
{
    public Transform lefDoor;
    public Transform rightDoor;
    public Transform leftClosedLocation;
    public Transform rightClosedLocation;
    public Transform leftOpenLocation;
    public Transform rightOpenLocation;

    public float speed = 1.0f;

    bool isOpening = false;
    bool isClosing = false;
    Vector3 distance;

    // Update is called once per frame
    void Update()
    {
        if (isOpening)
        {
            distance = lefDoor.localPosition -
                leftOpenLocation.localPosition;

            if (distance.magnitude < 0.001f)
            {
                isOpening = false;
                lefDoor.localPosition =
                    leftOpenLocation.localPosition;
                rightDoor.localPosition =
                    rightOpenLocation.localPosition;
            }
            else
            {
                lefDoor.localPosition =
                    Vector3.Lerp(lefDoor.localPosition,
                    leftOpenLocation.localPosition,
                    Time.deltaTime * speed);
                rightDoor.localPosition =
                    Vector3.Lerp(rightDoor.localPosition,
                    rightOpenLocation.localPosition,
                    Time.deltaTime * speed);
            }
        }
        else if (isClosing)
        {
            distance = lefDoor.localPosition -
                leftClosedLocation.localPosition;
            
            if (distance.magnitude < 0.001f)
            {
                isClosing = false;
                lefDoor.localPosition =
                    leftClosedLocation.localPosition;
                rightDoor.localPosition =
                    rightClosedLocation.localPosition;
            }
            else
            {
                lefDoor.localPosition =
                    Vector3.Lerp(lefDoor.localPosition,
                    leftClosedLocation.localPosition,
                    Time.deltaTime * speed);
                rightDoor.localPosition =
                    Vector3.Lerp(rightDoor.localPosition,
                    rightClosedLocation.localPosition,
                    Time.deltaTime * speed);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        isOpening = true;
        isClosing = false;
    }

    private void OnTriggerStay(Collider other)
    {
        isOpening = true;
        isClosing = false;
    }

    private void OnTriggerExit(Collider other)
    {
        isOpening = false;
        isOpening = true;
    }
}
