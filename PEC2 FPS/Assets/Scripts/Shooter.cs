﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject decalPrefab;
    public AudioSource fireSound;
    public float range = 100;
    public float damage = 10;
    public float fireRate = 15;

    private float nextTimeToFire = 0;
    GameObject[] totalDecals;
    int actual_decal = 0;
    private void Start()
    {
        totalDecals = new GameObject[10];
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire )
        {
            nextTimeToFire = Time.time + 1f/fireRate;
            Shoot();
        }
        
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(
            Camera.main.ViewportPointToRay(
                new Vector3(0.5f, 0.5f, 0)), out hit))
        {
            Health targetHealt = hit.transform.GetComponent<Health>();
            if (targetHealt != null)
                targetHealt.TakeDamage(damage);
            else
            {
                Destroy(totalDecals[actual_decal]);
                totalDecals[actual_decal] = GameObject.Instantiate(decalPrefab,
                    hit.point + hit.normal * 0.01f,
                    Quaternion.FromToRotation(Vector3.forward,
                    -hit.normal));
                actual_decal++;
            }
            fireSound.Play();
            if (actual_decal == 10) actual_decal = 0;
        }
    }
}
